require('./bootstrap');

const app = new Vue({
    el: '#app',
    data: {
        phone_list: null,
        text: null,
        parsed_text: null,
        phone_text: null,
        prefix: '',
        template_vars: [],
        submitingError: null
    },
    methods: {
        uploadFile(event){
            let files = event.target.files || event.dataTransfer.files;
            let file = files[0];

            let promise = new Promise((resolve, reject) => {
                let reader;

                try {
                    reader = new FileReader();
                } catch (e) {
                    message='FileReader function not implement in this web browser';
                    reject(message);
                    this.submitingError=message;
                }

                try {
                    reader.readAsText(file);
                    reader.onload = (e) => {
                        let csv = e.target.result;
                        let lines = this.parseTextPhonesLines(csv);
                        resolve(lines);
                    };
                } catch (e) {
                    message='Cannot read the file';
                    reject(message);
                    this.submitingError=message;
                }
            });

            promise.then((lines) => {
                this.phone_list = lines;

            })
            .catch((message) => {
                console.log(message);
            });

        },

        parseTextPhonesLines(text){
            let alltextlines = text.split(/\r\n|\n/);
            let lines = [];
            while(alltextlines.length){
                lines.push(alltextlines.shift().split(/,|\t/));
            }

            return lines;
        },

        parsePhone(phone){
            let re = /[0-9]+/g
            let found = phone.match(re);
            return found[0];
        },

        parseText(line){
            if(line.length > 1){
                let re = /\$col[0-9]+/;
                let found = this.text.match(re);

                if(found.length > 0){

                }
            }
        }



    },
    watch: {
        phone_text : function(val) {
            let lines = this.parseTextPhonesLines(val);
            this.phone_list = lines;

        }
    }

});
