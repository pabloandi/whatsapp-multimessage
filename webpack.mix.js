const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.autoload({
    vue: ['Vue','window.Vue'],
    querystring: ['querystring','window.querystring']
});

mix.setPublicPath('./public');

//javascript
mix.js('resources/js/app.js', 'public/js');
mix.extract(['vue','querystring']);

//css
mix.sass('resources/sass/vendor.scss','public/css/vendor.css');
mix.sass('resources/sass/app.scss','public/css/app.css');
